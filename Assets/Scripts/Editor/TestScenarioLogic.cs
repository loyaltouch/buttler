﻿using NUnit.Framework;
using Assert = UnityEngine.Assertions.Assert;
using System.Collections;
using System.Collections.Generic;

public class TestScenarioLogic
{
    string xmlText1;
    string xmlText2;
    string xmlText3;
    string csvText;

    [SetUp]
    public void SetUp()
    {
        xmlText1 = @"<scene>
<flag key=""$scene.enemy"" value=""$scene.selected""/>
<message>$enm.name が現れた！</message>
<select scene=""initiative"" >戦う</select>
</scene>";

        xmlText2 = @"<scene>
  <flag key=""$scene.youAtk"" value=""$you.dex :2D6 +""/>
  <flag key=""$scene.enmAtk"" value=""$enm.dex :2D6 +""/>
  <flag cond=""$scene.youAtk $scene.enmAtk :le"" key=""$turn.actor"" value=""$you.name"" />
  <flag cond=""$scene.youAtk $scene.enmAtk :gt"" key=""$turn.actor"" value=""$enm.name"" />
  <message>$you.name の攻撃力 : $scene.youAtk 
$enm.name の攻撃力 : $scene.enmAtk 
$turn.actor の攻撃</message>
  <select scene=""atkTurn"" value=""2"">次へ</select>
  <select scene=""tryLuck"">運試しをする</select>
</scene>";

        xmlText3 = @"<scene>
  <flag key=""$you.lck"" value=""1 $you.lck -"" />
</scene>";

        csvText = @"name,vit,dex,lck,image
you,12,6,9,
スライム,6,4,0,slime
ゾンビ,10,4,0,zombie
スケルトン,8,6,0,scelton
幽霊,7,6,0,ghost
グール,5,5,0,gool";
    }

    [Test]
    public void testParseScene3()
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        Assert.AreEqual("9", sl.GameData["$you.lck "]);
        sl.parseScene(xmlText3, "", "");
        Assert.AreEqual("8", sl.GameData["$you.lck "]);
    }

    [Test]
    public void testParseMemberData()
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        Assert.AreEqual("スライム", sl.MemberData["スライム"]["name"]);
        Assert.AreEqual("9", sl.MemberData["you"]["lck"]);
        Assert.AreEqual("gool", sl.MemberData["グール"]["image"]);
    }

    [Test]
    public void testParseScene()
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        sl.parseScene(xmlText1, "スライム", "");
        Assert.AreEqual("スライムが現れた！", sl.Message);
        Assert.AreEqual("戦う", sl.selectors[0].Label);
        Assert.AreEqual("initiative", sl.selectors[0].Scene);
    }

    [Test]
    public void testParseScene2()
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        sl.parseScene(xmlText2, "スケルトン", "");
        Assert.AreEqual("次へ", sl.selectors[0].Label);
        Assert.AreEqual("atkTurn", sl.selectors[0].Scene);
        Assert.AreEqual("2", sl.selectors[0].Value);
        Assert.AreEqual("運試しをする", sl.selectors[1].Label);
        Assert.AreEqual("tryLuck", sl.selectors[1].Scene);
        Assert.AreEqual("", sl.selectors[1].Value);
    }

    [TestCase("2 3 +", "5")]
    [TestCase("2 3 -", "1")]
    [TestCase("2 3 :gt", "1")]
    [TestCase("3 3 :gt", "0")]
    [TestCase("3 3 :ge", "1")]
    [TestCase("3 2 :le", "1")]
    [TestCase("$you.vit", "12")]
    [TestCase("$you.name", "プレイヤー")]
    public void testParseTokens(string text, string result)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        Assert.AreEqual(result, sl.parseTokens(text));
    }

    [TestCase("あなたは$you.name です", "あなたはプレイヤーです")]
    public void testVarExpand(string text, string result)
    {
        var sl = new ScenarioLogic();
        sl.parseMemberData(csvText);
        Assert.AreEqual(result, sl.varExpand(text));
    }
}
