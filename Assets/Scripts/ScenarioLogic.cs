using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Text.RegularExpressions;

public class ScenarioLogic
{
    private Dictionary<string, string> gameData = new Dictionary<string, string>();
    public Dictionary<string, string> GameData { get { return gameData; } }

    private Dictionary<string, Dictionary<string, string>> memberData;
    public Dictionary<string, Dictionary<string, string>> MemberData { get { return memberData; } }
    public string strDebugger = "";

    public List<Selector> selectors = new List<Selector>();

    public string Message { get; set; }

    public void parseScene(string xmlText, string selLabel, string selValue)
    {
        // シーンフラグをクリア
        clearFlagSection("scene");

        // 直前の情報をシーンセクションの変数に保持
        gameData["$scene.selected "] = selLabel;
        gameData["$scene.selvalue "] = parseTokens(selValue);
        gameData["$scene.prevmessage "] = Message;

        var rootXml = new XmlDocument();
        rootXml.LoadXml(xmlText);
        var sceneXml = rootXml.SelectSingleNode("scene");

        // フラグ処理
        flag2GameData(sceneXml);

        // メッセージ構成
        Message = createMessage(sceneXml);

        // 選択肢の作成
        selectors = createSelectors(sceneXml);
    }

    private void flag2GameData(XmlNode sceneNode)
    {
        foreach (XmlNode node in sceneNode.SelectNodes("flag"))
        {
            if (checkCondition(node))
            {
                string key = node.Attributes["key"].Value;
                string value = node.Attributes["value"].Value;
                gameData[key + " "] = parseTokens(value);
            }
        }
        // 敵遭遇処理
        processEncounter();

        // 宿屋処理
        processInn();

        // 運試し処理
        processTryLuck();

        // ダメージ処理
        processDamage();
    }

    private string createMessage(XmlNode sceneNode)
    {
        string result = "";
        foreach (XmlNode node in sceneNode.SelectNodes("message"))
        {
            if (checkCondition(node))
            {
                result += varExpand(node.InnerText);
            }
        }
        return result;
    }

    private List<Selector> createSelectors(XmlNode sceneNode)
    {
        var result = new List<Selector>();
        foreach (XmlNode node in sceneNode.SelectNodes("select"))
        {
            if (checkCondition(node))
            {
                var selector = new Selector();
                selector.Label = node.InnerText;
                selector.Scene = node.Attributes["scene"].Value;
                if (node.Attributes["value"] != null)
                {
                    selector.Value = node.Attributes["value"].Value;
                }
                result.Add(selector);
            }
        }
        return result;
    }

    private void processEncounter()
    {
        var enemyName = "";
        if (gameData.TryGetValue("$scene.enemy ", out enemyName))
        {
            setupEnemyData(enemyName);
        }
        gameData["$scene.enemy "] = "";
    }

    private void processDamage()
    {
        string damageStr = null;
        gameData.TryGetValue("$scene.damage ", out damageStr);

        string actor = null;
        gameData.TryGetValue("$turn.actor ", out actor);

        int damage = atoi(damageStr);
        if (damage != 0)
        {
            if (actor == "プレイヤー")
            {
                gameData["$enm.vit "] = (atoi(gameData["$enm.vit "]) - damage).ToString();
            }
            else
            {
                gameData["$you.vit "] = (atoi(gameData["$you.vit "]) - damage).ToString();
            }
            gameData["$scene.damageEffect "] = actor;
        }
        gameData["$scene.damage "] = "";
    }

    private void processTryLuck()
    {
        string luckResult = null;
        gameData.TryGetValue("$scene.result ", out luckResult);

        string actor = null;
        gameData.TryGetValue("$turn.actor ", out actor);

        if (luckResult == "0")
        {
            if (actor == "プレイヤー")
            {
                gameData["$turn.luckresult "] = "1";
            }
            else
            {
                gameData["$turn.luckresult "] = "3";
            }
        }
        else
        {
            if (actor == "プレイヤー")
            {
                gameData["$turn.luckresult "] = "3";
            }
            else
            {
                gameData["$turn.luckresult "] = "1";
            }
        }
    }

    private void processInn()
    {
        string inn = null;
        gameData.TryGetValue("$scene.inn ", out inn);
        if (inn == "1")
        {
            setupPlayerData();
        }
        gameData["$scene.inn "] = "";
    }


    public void clearFlagSection(string section)
    {
        var keys = new List<string>();
        foreach (var key in gameData.Keys)
        {
            keys.Add(key);
        }

        foreach (var key in keys)
        {
            if (Regex.IsMatch(key, string.Format(@"^\${0}\.", section)))
            {
                gameData[key] = "";
            }
        }
    }

    public string parseTokens(string text)
    {
        var stack = new Stack<string>();
        foreach (var token in Regex.Split(text, " +"))
        {
            switch(token){
                case "+" :
                    stack.Push((atoi(stack.Pop()) + atoi(stack.Pop())).ToString());
                    break;
                case "-":
                    stack.Push((atoi(stack.Pop()) - atoi(stack.Pop())).ToString());
                    break;
                case "*":
                    stack.Push((atoi(stack.Pop()) * atoi(stack.Pop())).ToString());
                    break;
                case "/":
                    stack.Push((atoi(stack.Pop()) / atoi(stack.Pop())).ToString());
                    break;
                case "%":
                    stack.Push((atoi(stack.Pop()) % atoi(stack.Pop())).ToString());
                    break;
                case "==":
                    stack.Push(stack.Pop() == stack.Pop() ? "1" : "0");
                    break;
                case ":gt":
                    stack.Push(atoi(stack.Pop()) > atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":ge":
                    stack.Push(atoi(stack.Pop()) >= atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":lt":
                    stack.Push(atoi(stack.Pop()) < atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":le":
                    stack.Push(atoi(stack.Pop()) <= atoi(stack.Pop()) ? "1" : "0");
                    break;
                case ":not":
                    stack.Push(stack.Pop() == "0" ? "1" : "0");
                    break;
                case ":2D6":
                stack.Push((Random.Range(1, 7) + Random.Range(1, 7)).ToString());
                    break;
                case ":1D6":
                    stack.Push((Random.Range(1, 7) + Random.Range(1, 7)).ToString());
                    break;
                default:
                if (token.StartsWith("$"))
                {
                    var result = "";
                    if (gameData.TryGetValue(token + " ", out result))
                    {
                        stack.Push(result);
                    }
                    else
                    {
                        stack.Push("");
                    }
                }
                else
                {
                    stack.Push(token);
                }
                break;
            }
        }
        return stack.Pop();
    }

    public void parseMemberData(string csvText)
    {
        var lines = csvText.Split('\n');
        var headers = new List<string>();
        memberData = new Dictionary<string, Dictionary<string, string>>();

        for (var i = 0; i < lines.Length; i++)
        {
            var cols = lines[i].Replace("\r", "").Split(',');
            if (i == 0)
            {
                headers.AddRange(cols);
            }
            else
            {
                var entry = new Dictionary<string, string>();
                for (var j = 0; j < cols.Length; j++)
                {
                    entry[headers[j]] = cols[j];
                }
                memberData[entry["name"]] = entry;
            }
        }
        setupPlayerData();
        gameData["$enm.name "] = "";
    }

    private void setupPlayerData()
    {
        try
        {
            gameData["$you.name "] = "プレイヤー";
            gameData["$you.vit "] = memberData["you"]["vit"];
            gameData["$you.dex "] = memberData["you"]["dex"];
            gameData["$you.lck "] = memberData["you"]["lck"];
        }
        catch (KeyNotFoundException)
        {
        }
    }

    private void setupEnemyData(string name)
    {
        try
        {
            gameData["$enm.name "] = memberData[name]["name"];
            gameData["$enm.vit "] = memberData[name]["vit"];
            gameData["$enm.dex "] = memberData[name]["dex"];
            gameData["$enm.image "] = memberData[name]["image"];
        }
        catch (KeyNotFoundException)
        {
        }
    }

    public string varExpand(string text)
    {
        return Regex.Replace(text, @"\$.+? ", varExpand_func);
    }

    private string varExpand_func(Match match)
    {
        var result = "";
        if (gameData.TryGetValue(match.Value, out result))
        {
            return result;
        }
        return "";
    }

    private bool checkCondition(XmlNode node)
    {
        if (node.Attributes["cond"] == null)
        {
            return true;
        }
        return atoi(parseTokens(node.Attributes["cond"].Value)) > 0;
    }

    private int atoi(string value)
    {
        try
        {
            return int.Parse(value);
        }
        catch
        {
            return 0;
        }
    }
}

public class Selector
{
    public string Label { get; set; }
    public string Scene { get; set; }
    public string Value { get; set; }

    public Selector()
    {
        Label = "";
        Scene = "";
        Value = "";
    }
}