﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainLogic : MonoBehaviour
{
    public TextMeshProUGUI ui_you_name;
    public TextMeshProUGUI ui_you_vit_max;
    public TextMeshProUGUI ui_you_vit_now;
    public TextMeshProUGUI ui_you_dex;
    public TextMeshProUGUI ui_you_lck_max;
    public TextMeshProUGUI ui_you_lck_now;
    public TextMeshProUGUI ui_enm_name;
    public TextMeshProUGUI ui_enm_vit_max;
    public TextMeshProUGUI ui_enm_vit_now;
    public TextMeshProUGUI ui_enm_dex;
    public TextMeshProUGUI ui_message;
    public GameObject EnemyImage;
    public Button[] ui_fkeys;

    public Image ui_you_status;
    public Image ui_enm_status;

    Dictionary<string, Dictionary<string, string>> enemyData = new Dictionary<string, Dictionary<string, string>>();
    string[] _selects = new string[5];
    List<string> enemyNames;

    ScenarioLogic sl = new ScenarioLogic();

    // Start is called before the first frame update
    void Start()
    {
        initData();
        var initSelect = new Selector();
        initSelect.Scene = "selenemy";
        sl.selectors.Add(initSelect);
        doSelect(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ui_fkeys[0].onClick.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            ui_fkeys[1].onClick.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            ui_fkeys[2].onClick.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            ui_fkeys[3].onClick.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            ui_fkeys[4].onClick.Invoke();
        }
    }

    void initData()
    {
        var enemyText = Resources.Load<TextAsset>("enemydata");
        sl.parseMemberData(enemyText.text);
    }

    void doSelect(int selcount)
    {
        if(sl.selectors.Count > selcount)
        {
            var select = sl.selectors[selcount];
            var sceneText = Resources.Load<TextAsset>("scenario/" + select.Scene);
            sl.parseScene(sceneText.text, select.Label, select.Value);
            updateAll();
        }
    }

    void updateAll()
    {
        updateStatus();
        updateMessage();
        updateSelects();
        updateImage();
    }

    void updateImage()
    {
        try
        {
            var sp = Resources.Load<Sprite>(sl.GameData["$enm.image "]);
            EnemyImage.GetComponent<SpriteRenderer>().sprite = sp;
        }
        catch(KeyNotFoundException)
        {

        }
    }

    void updateStatus()
    {
        ui_you_vit_max.text = sl.MemberData["you"]["vit"];
        ui_you_vit_now.text = sl.GameData["$you.vit "];
        ui_you_dex.text = sl.GameData["$you.dex "];
        ui_you_lck_max.text = sl.MemberData["you"]["lck"];
        ui_you_lck_now.text = sl.GameData["$you.lck "];

        var enmName = sl.GameData["$enm.name "];
        if(enmName != "")
        {
            ui_enm_name.text = enmName;
            ui_enm_vit_max.text = sl.MemberData[enmName]["vit"];
            ui_enm_vit_now.text = sl.GameData["$enm.vit "] ;
            ui_enm_dex.text = sl.MemberData[enmName]["dex"];
        }

        string effect = "";
        if(sl.GameData.TryGetValue("$scene.damageEffect ", out effect))
        {
            if(effect == "プレイヤー")
            {
                StartCoroutine(damageEffect(ui_enm_status));
            }
            else if(effect != "")
            {
                StartCoroutine(damageEffect(ui_you_status));
            }
        }
    }

    void updateMessage()
    {
        ui_message.text = sl.Message;
    }

    void updateSelects()
    {
        for (int i = 0; i < 5; i++)
        {
            _selects[i] = "";
        }

        int counter = 0;
        foreach(var selector in sl.selectors)
        {
            _selects[counter] = selector.Label;
            counter++;
        }
        for(int i = 0; i < 5; i++)
        {
            ui_fkeys[i].transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>().text = string.Format("F{0}: {1}", i + 1, _selects[i]);
        }
    }

    IEnumerator damageEffect(Image target)
    {
        var oldPos = target.transform.position;
        target.transform.position = new Vector3(oldPos.x - 10, oldPos.y - 2, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x + 10, oldPos.y - 4, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x - 10, oldPos.y - 6, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x + 10, oldPos.y - 8, oldPos.z);
        yield return null;
        target.transform.position = new Vector3(oldPos.x, oldPos.y, oldPos.z);
    }

    public void onF1Click()
    {
        doSelect(0);
    }
    public void onF2Click()
    {
        doSelect(1);
    }
    public void onF3Click()
    {
        doSelect(2);
    }
    public void onF4Click()
    {
        doSelect(3);
    }
    public void onF5Click()
    {
        doSelect(4);
    }
}
